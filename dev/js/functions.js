$(function () {
    $.getJSON(
        "https://api.instagram.com/v1/users/self/media/recent/?access_token=11896475376.1677ed0.56df0f5c823a4ee8b48201bf0d24b496 &callback=?",
        function (insta) {
            $.each(insta.data, function (photos, src) {
                if (photos === 5) {
                    return false;
                }
                $(
                    '<div> <a target="_blank" href="' + src.link + '"><img src="' + src.images.standard_resolution.url + '"></a> <p> <span class="c-instagram__like">' + src.likes.count + '</span> <span class="c-instagram__comment">' + src.comments.count + '</span> </p> </div>'
                ).appendTo(".c-instagram__content");
            });
        }
    );
});
$('.c-header__lag p').on("click", function () {
    $(this).toggleClass('opened');
    $('.c-header__lag ul').slideToggle('300ms');
});
