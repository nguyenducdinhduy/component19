<?php /*========================================
list
================================================*/ ?>
<div class="c-dev-title1">list</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list1</div>
<div class="l-container">
    <div class="c-list1">
        <h2 class="c-title3">ATRE FLOOR GUIDE</h2>
        <ul>
            <li>
                <p><a href="#">2F</a></p>
                <h3>CAFÉ & RESTAURANT<br>FASHION & VARIETY FOODS</h3>
            </li>
            <li>
                <p><a href="#">3F</a></p>
                <h3>VARIETY GOODS & FOODS<br>CAFÉ & RESTAURANT</h3>
            </li>
            <li>
                <p><a href="#">4F</a></p>
                <h3>VARIETY GOODS & FOODS<br>CAFÉ & RESTAURANT</h3>
            </li>
        </ul>
    </div>
</div>
<div class="c-dev-title2">c-list2</div>
<div class="l-container">
    <ul class="c-list2">
        <li>
            <img src="/assets/img/common/icon3.png" alt="MRT">
            <h3>-MRT-</h3>
            <dl>
                <dt>・板南線「市政府駅」2番出口徒歩約10分</dt>
                <dt>・信義線「台北101駅」4番出口徒歩約3分</dt>
            </dl>
        </li>
        <li>
            <img src="/assets/img/common/icon4.png" alt="バス">
            <h3>-バス-</h3>
            <p>
                市政府、世貿センター、信義行政センター、
                <br>グランドハイアットホテル路線のバスで
                <br>台北101バス停下車
            </p>
        </li>
    </ul>
</div>