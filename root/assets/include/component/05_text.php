<?php /*========================================
text
================================================*/ ?>
<div class="c-dev-title1">text</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-text1</div>
<div style="padding-top:50px;padding-bottom:50px;background:#f3f3f3">
    <ul class="c-text1">
        <li>
            <p class="c-text1__tit"><span>住所</span></p>
            <p class="c-text1__text">台北市信義區松智路17號</p>
        </li>
        <li>
            <p class="c-text1__tit"><span>營業時間</span></p>
            <p class="c-text1__text">週日至週三　11:00 - 21:30<br>週四至週六 (含例假日前夕)　11:00 - 22:00</p>
            <p class="c-text1__note">※部分專櫃時間調整</p>
        </li>
    </ul>
</div>