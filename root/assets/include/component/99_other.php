<?php /*========================================
other
================================================*/ ?>
<div class="c-dev-title1">other</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-visual</div>
<div class="c-visual">
    <div class="l-container">
        <h2 class="c-title1"><span>FLOOR GUIDE</span></h2>
        <div class="c-visual__box">
            <ul>
                <li class="active"><a href="#">2F</a></li>
                <li><a href="#">3F</a></li>
                <li><a href="#">4F</a></li>
            </ul>
            <div class="c-visual__tt">
                <p>2F</p>
                <h1>CAFÉ &amp; RESTAURANT<br>FASHION &amp; VARIETY FOODS</h1>
            </div>
        </div>
    </div>
</div>
<div class="c-dev-title2">c-facebook</div>
<div class="l-container">
    <div class="fb-page" data-href="https://www.facebook.com/atre.tw/" data-tabs="timeline" data-width="368" data-height="527" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
        <blockquote cite="https://www.facebook.com/atre.tw/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/atre.tw/">atre編集部</a></blockquote>
    </div>
</div>
<div class="c-dev-title2">c-instagram</div>
<div class="l-container">
    <div class='c-instagram'>
        <h2 class="c-title2">SNAP SHOT on INSTAGRAM</h2>
        <div class="c-instagram__box">
            <div class="c-instagram__logo">
                <a href="https://www.instagram.com/atresnap_taipei/" target="_blank"><img src="/assets/img/common/logo2.jpg" alt="atré SNAP">atresnap_taipei</a>
            </div>
            <div class="c-instagram__content"></div>
            <div class="c-instagram__bnt"><a target="_blank" href="https://www.instagram.com/atresnap_taipei/">在 Instagram 上扎中</a></div>
        </div>
    </div>
</div>