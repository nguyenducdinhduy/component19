<?php /*========================================
table
================================================*/ ?>
<div class="c-dev-title1">table</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-table</div>
<div style="padding-top:50px;padding-bottom:50px;background:#f3f3f3">
    <div class="l-container">
        <table class="c-table">
            <tr>
                <th>公司名</th>
                <td>微風艾妥列南山股份有限公司</td>
            </tr>
            <tr>
                <th>本公司所在地</th>
                <td>台北市信義區松智路17號3樓</td>
            </tr>
            <tr>
                <th>總経理</th>
                <td>伊藤浩平</td>
            </tr>
            <tr>
                <th>URL</th>
                <td><a href="#">http://atre.com.tw</a></td>
            </tr>
            <tr>
                <th>事業内容</th>
                <td>購物中心運営開発等</td>
            </tr>
            <tr>
                <th>關連會社</th>
                <td>三井物産(株)、(株)atre</td>
            </tr>
        </table>
    </div>
</div>

<div class="c-dev-title2">c-table c-table__color2</div>
<div class="l-container">
    <table class="c-table c-table__color2">
        <tr>
            <th>公司名</th>
            <td>株式會社atre</td>
        </tr>
        <tr>
            <th>会社設立</th>
            <td>1990年4月2日</td>
        </tr>
        <tr>
            <th>資本額</th>
            <td>16億3千日幣</td>
        </tr>
        <tr>
            <th>總公司地址</th>
            <td>〒150-0013　東京都涉谷區惠比壽4丁目1番18號　惠比壽NEONATO大樓6F</td>
        </tr>
        <tr>
            <th>董事長</th>
            <td>一ノ瀬 俊郎</td>
        </tr>
        <tr>
            <th>員工人數</th>
            <td>426名（2017年4月1日資料）</td>
        </tr>
        <tr>
            <th>公司網址</th>
            <td><a href="https://www.atre.co.jp">https://www.atre.co.jp</a></td>
        </tr>
        <tr>
            <th>主要業務內容</th>
            <td>與JR東日本共同開發車站大樓、車站商場的管理與營運、站內　開發企劃與營運承攬</td>
        </tr>
        <tr>
            <th>關係企業</th>
            <td>(株)Atre-Style、宇都宮車站開發(株)、高崎TERMINAL BUILDING(株)、水戶車站開發(株)</td>
        </tr>
    </table>
</div>