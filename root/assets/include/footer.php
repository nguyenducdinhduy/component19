<footer class="c-footer">
    <div class="c-footer__social">
        <div class="l-container">
            <ul>
                <li id="icon_fb">
                    <a href="https://www.facebook.com/atre.tw/">
                        <figure><img src="/assets/img/icon/facebook.svg" alt="Facebook"></figure>
                        <span>Facebook</span>
                    </a>
                </li>
                <li id="icon_ins">
                    <a href="https://www.instagram.com/atresnap_taipei/">
                        <figure><img src="/assets/img/icon/instagram.svg" alt="instagram"></figure>
                        <span>instagram</span>
                    </a>
                </li>
                <li id="icon_you">
                    <a href="#">
                        <figure><img src="/assets/img/icon/youtube.svg" alt="Youtube"></figure>
                        <span>Youtube</span>
                    </a>
                </li>
                <li id="icon_atr"><a href="https://www.atre.co.jp/" target="_blank">atre japan</a></li>
                <li id="icon_bree"><a href="https://www.breezecenter.com/" target="_blank">微風南山</a></li>
            </ul>
        </div>
    </div>
    <div class="c-footer__content">
        <div class="l-container">
            <ul class="c-footer__address">
                <li>
                    <div>營業時間</div>
                    <p>
                        週日至週三 11:00 - 21:30
                        <br>週四至週六 (含例假日前夕) 11:00 - 22:00
                        <br><span>※部分專櫃時間調整</span>
                    </p>

                </li>
                <li>
                    <div>地 址</div>
                    <p>台北市信義區松智路17號</p>
                </li>
            </ul>
            <div class="c-footer__logo">
                <div><img src="/assets/img/common/logo.png" alt="atré"></div>
                <p class="c-footer__copyright">&copy; Breeze atre Holding Co., Ltd. 2018～</p>
            </div>
        </div>
    </div>
</footer>
<script src="/assets/js/functions.min.js"></script>
</body>

</html>