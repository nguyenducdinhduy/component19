<!DOCTYPE html>
<html lang="ja" id="pagetop">

<head>
    <meta charset="UTF-8">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width">
    <?php include('meta.php'); ?>
    <link href="/assets/css/atre-ch/style.css" rel="stylesheet">
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="/assets/css/slick.css" rel="stylesheet">
    <link href="/assets/css/lightbox.css" rel="stylesheet">
    <link href="/assets/css/style.min.css" rel="stylesheet">
    <script src="/assets/js/jquery-3.3.1.min.js"></script>
    <script src="/assets/js/jquery-migrate-3.0.1.min.js"></script>
</head>

<body class="page-<?php echo $pageid; ?>">
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v3.3&appId=2597687480260139&autoLogAppEvents=1"></script>
    <header class="c-header">
        <div class="c-header__content">
            <div class="l-container">
                <div class="c-header__logo"><a href="/"><img src="/assets/img/common/logo.png" alt="atré"></a></div>
                <div class="c-header__lag">
                    <p>Language</p>
                    <ul>
                        <li><a href="/">CH</a></li>
                        <li><a href="/">JP</a></li>
                    </ul>
                </div>
                <ul class="c-header__social">
                    <li><a href="https://www.facebook.com/atre.tw/"><img src="/assets/img/common/social3.png" alt="Facebook"></a></li>
                    <li><a href="https://www.instagram.com/atresnap_taipei/"><img src="/assets/img/common/social2.png" alt="Instagram"></a></li>
                    <li><a href="#"><img src="/assets/img/common/social1.png" alt="Youtube"></a></li>
                </ul>
            </div>
        </div>
        <nav class="c-header__nav">
            <ul>
                <li><a href="#">最新消息</a></li>
                <li><a href="#">樓層介紹</a></li>
                <li><a href="#">設施簡介</a></li>
                <li><a href="#">關於atre</a></li>
            </ul>
        </nav>
    </header>